library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity  concat32 is

	generic
	(
		DATA_WIDTH : natural := 8	-- datA_WIDTH equivale ao tamanho da largura de palavra, que é o numero de estágios do shift
	);

	port 
	(
		concat_uc		: in std_logic;
		reg1				: in std_logic_vector((DATA_WIDTH-1) downto 0);
		reg2				: in std_logic_vector((DATA_WIDTH-1) downto 0);
		reg3				: in std_logic_vector((DATA_WIDTH-1) downto 0);
		reg4				: in std_logic_vector((DATA_WIDTH-1) downto 0);
		concat_output	: out std_logic_vector(((DATA_WIDTH*4)-1) downto 0)
	);

end entity;

architecture rtl of concat32 is

	
begin
	process (reg1, reg2, reg3, reg4)
		begin
		if(concat_uc = '1') then
			concat_output(31 downto 24) 	<= reg1;
			concat_output(23 downto 16) 	<= reg2;
			concat_output(15 downto 8) 	<= reg3;
			concat_output(7 downto 0) 		<= reg4;
		else
			concat_output(31 downto 24)	<= "00000000";
			concat_output(23 downto 16)	<= "00000000";
			concat_output(15 downto 8)		<= reg1;
			concat_output(7 downto 0)		<= reg2;	
		end if;
	end process;
end rtl;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ula is
	generic
	(
		DATA_WIDTH : natural := 8 
	);
	port 
	(
		input_a	: in std_logic_vector ((DATA_WIDTH-1) downto 0);
		input_b	: in std_logic_vector ((DATA_WIDTH-1) downto 0);
		-- Gaio: Da pra escrever (não sei se é essa mesmo a correspondencia)
		-- add : 00
		-- sub : 01
		-- mul : 10
		-- div : 11
		addSubMul: in std_logic_vector (1 downto 0);
		result 	: out std_logic_vector (((DATA_WIDTH)-1) downto 0);
		flag 		: out std_logic_vector (1 downto 0)
	);

end entity;

architecture rtl of ula is
begin
	process(input_a, input_b, addSubMul)
	begin
		-- Add if "add_sub" is 1, else subtract
		-- 00 maior
		-- 01 menor
		-- 10 igual
		-- 11 invalido
		if (addSubMul = "00") then
			result 	<= std_logic_vector(unsigned(input_a) + unsigned(input_b));
			flag		<= "00";
		elsif (addSubMul = "01") then
			result <= std_logic_vector(unsigned(input_a) - unsigned(input_b));
			if(unsigned(input_a) < unsigned(input_b)) then
				flag <= "01";
			elsif(unsigned(input_a) = unsigned(input_b)) then
				flag <= "10";
			else
				flag <= "00";
			end if;
		elsif (addSubMul = "10") then
			result <= std_logic_vector(unsigned(input_a) * unsigned(input_b))(((DATA_WIDTH)-1) downto 0);
			if(unsigned(std_logic_vector(unsigned(input_a)* unsigned(input_b))(((DATA_WIDTH+8)-1) downto 8 ))=0) then
				flag <= "11";
			else
				flag <= "00";
			end if;
		else
			if(unsigned(input_b) = 0) then
				flag <= "11";
			else
				flag <= "00";
			end if;
			result <= std_logic_vector(unsigned(input_a) / unsigned(input_b))(((DATA_WIDTH)-1) downto 0);
		end if;
	end process;
end rtl;
-- Quartus II VHDL Template
-- Four-State Mealy State Machine

-- A Mealy machine has outputs that depend on both the state and
-- the inputs.	When the inputs change, the outputs are updated
-- immediately, without waiting for a clock edge.  The outputs
-- can be written more than once per state or per clock cycle.

library ieee;
use ieee.std_logic_1164.all;

entity controle is
	port
	(
		clk										: in	std_logic;								-- clock geral arquitetura
		input	 							      : in	std_logic_vector(7 downto 0);		-- binário da instrução que será executada
		reset	 							      : in	std_logic;								-- reset geral arquitetura
		ula_input 						      : in	std_logic_vector(1 downto 0);		-- qual operadorentra na ula
		mux_add_uc1, mux_add_uc2 	      : out	std_logic;								-- bits de seleÃ§Ã£o do mux antes do PC
		reg_pc_uc						      : out	std_logic;								-- enable registrador PC, (ele ja controle a memÃ³ria de programa)
		reg_d1_uc						      : out std_logic;								-- enable registrador D1
		reg_d2_uc						      : out	std_logic;								-- enable registrador D2
		reg_d3_uc						      : out	std_logic;								-- enable registrador D3
		reg_d4_uc						      : out	std_logic;								-- enable registrador D4
		ula_uc							      : out	std_logic_vector(1 downto 0);		-- seleÃ§Ã£o de operaÃ§Ã£o que ula vai realizar
		concat_uc								: out	std_logic;								-- sinais de controle do concatenador
		enable_pilha					      : out	std_logic;								-- habilita e desabilita retirada/inserÃ§Ã£o na pilha
		PUSH_barPOP						      : out	std_logic;								-- seleÃ§Ã£o da operaÃ§Ã£o: se push ou pop
		mux_4_uc1, mux_4_uc2			      : out	std_logic;								-- seleÃ§Ã£o de entrada do mux que esta antes do mux_8
		mux_8_uc1, mux_8_uc2, mux_8_uc3	: out	std_logic;								-- sinais de seleÃ§Ã£o de entrada do mux_8 que entra na pilha
		men_dados_uc							: out	std_logic;
		read_enable_data_uc					: out	std_logic;
		read_enable_memory_uc				: out	std_logic;
		r2							 				: out	std_logic_vector(7 downto 0);		-- para mostrar externamente op code da instrução sendo executada
		rstatus									: out	std_logic_vector(7 downto 0)
	);

end entity;

architecture rtl of controle is
	-- tipo enumerado de todos os estados existentes
	type state_type is (cr, c0, c1, c2, c3, c4, c5, c6, c7, c10, c20,c30,c31,c32,c33);
	-- Register to hold the current state
	signal state 			  										: state_type;
	signal reg_instrucao, reg_ContStatus, input_status	: std_logic_vector(7 downto 0);
	signal reg_uc 													: std_logic;	

begin

	r2			<=	reg_instrucao;								-- reg_instrução armazena a instrução sendo atualmente processada
	rstatus	<=	input_status;								-- rstatus é o pino de saida do estado atual, input_status é o sinal interno que o controle usa

	--Registador Da instrução	
	process(clk)
		begin
			if clk'event and clk = '1' then
				if (reg_uc = '1') then							-- sinal de controle
					reg_instrucao <= input;
				end if;
			end if;
	end process;
		
	-- Registador contador,  para saber em que parte da instrução esta
	process(clk)
		begin
			if clk'event and clk = '1' then					-- sinal de controle
				reg_ContStatus <= input_status;				-- armazena qual etapa da instrução esta sendo executada
			end if;
	end process;
	
	-- lógica de transição de estados
	process (clk, reset)
	begin
		if (rising_edge(clk)) then
			if reset = '1' then
				state <= cr;									-- cr é o estado de reset
			else
				case state is
					when cr =>
						state	<= c0;
						input_status <= "00000000";
					when c0 =>
						if(reg_ContStatus = "00000000") then
							state <= c1;
						elsif(reg_ContStatus = "00000001" and reg_instrucao = "00010000") then		-- bipush
							input_status 	<= "00000010";
							state 			<= c2;
						elsif(reg_ContStatus = "00000001" and reg_instrucao = "01100000" ) then
							input_status <= "00000010";
							state 		 <= c7;
							--4
						elsif(reg_ContStatus = "00000011" and reg_instrucao = "01100000" ) then
							input_status <= "00000100";
							
							state 		 <= c32;
						elsif(reg_ContStatus = "00000111" and reg_instrucao = "01100000" ) then
							input_status <= "00001000";
							state 		 <= c33;
						end if;
					when c1 =>
						input_status <= "00000001";
						state <= c0;
					when c2 =>	-- ciclo bolha
						state 			<= c10;
					when c3 => -- bipush
						if(reg_ContStatus = "00000100" and reg_instrucao = "00010000" ) then
							input_status <= "11111111";	-- 255
							state 		 <= c2;
						elsif(reg_ContStatus = "00000011" and reg_instrucao = "00010000" ) then
							input_status <= "11111111";
							state 		 <= c2;
						end if;
					when c4 =>																							-- do c4 ao c7 gravar valores nos registradores da saida da pilha
						if(reg_ContStatus = "00000011" and reg_instrucao = "00010000" ) then
							input_status <= "00000000";
							state 		 <= c0;
						end if;
					when c5 =>
						if(reg_ContStatus = "00000011" and reg_instrucao = "00010000" ) then
							input_status <= "00000000";
							state 		 <= c0;
						end if;
					when c6 => --6
						if(reg_ContStatus = "00000111" and reg_instrucao = "01100000" ) then
							input_status <= "00001000";
							state 		 <= c20;
						elsif(reg_ContStatus = "00000101") then
							input_status <= "00000110";
							state 		 <= c6;
						elsif(reg_ContStatus = "00000110") then
							input_status <= "00000111";
							state 		 <= c6;
						end if;
					when c7 => --3
						if(reg_ContStatus = "00000100" and reg_instrucao = "01100000" ) then
							input_status <= "00000101";
							state 		 <= c6;
						elsif(reg_ContStatus = "00000010") then
							input_status <= "00000011";
							state 		 <= c7;
						elsif(reg_ContStatus = "00000011") then
							input_status <= "00000100";
							state 		 <= c7;
						
						end if;
					when c10 =>
						if(reg_ContStatus = "00000010" and reg_instrucao = "00010000" ) then	-- bipush
							input_status 	<= "00000011";
							state 			<= c3;
						elsif(reg_ContStatus = "11111111") then	-- ultima da instrução (generico para todas)
							input_status <= "00000000";
							state <= c0;
						end if;
					when c20 =>
						if(reg_ContStatus = "00001000" and reg_instrucao = "01100000" ) then
							input_status <= "11111111";
							state 		 <= c2;
						end if;
					when c30 =>
						if(reg_ContStatus = "00000101" and reg_instrucao = "01100000" ) then
							input_status <= "00000110";
							state 		 <= c6;
						end if;
					when c31 =>
						if(reg_ContStatus = "00001001" and reg_instrucao = "01100000" ) then
							input_status <= "00001010";
							state 		 <= c20;
						end if;
					when c32 =>
							if(reg_ContStatus = "00000100" and reg_instrucao = "01100000" ) then
							input_status <= "00000101";
							state 		 <= c30;
							end if;
					when c33 =>
						if(reg_ContStatus = "000001000" and reg_instrucao = "01100000" ) then
							input_status <= "00001001";
							state 		 <= c31;
							end if;
				end case;
			end if;
		end if;
	end process;

	-- lógica de geração dos sinais de saida
	process (state, input)
	begin
		case state is
			-- RESET
			when cr =>
					mux_add_uc1 			 	<= '0';
					mux_add_uc2   			   <= '1';
					reg_pc_uc 	  			   <= '1';
					reg_d1_uc 	 			   <= '0';
					reg_d2_uc 	  			   <= '0';
					reg_d3_uc 	  			   <= '0';
					reg_d4_uc 	 			   <= '0';
					ula_uc 		  			   <= "00";
					concat_uc 	 			   <= '0';
					enable_pilha  			   <= '0';
					PUSH_barPOP  			   <= '0';
					mux_4_uc1 	 			   <= '0';
					mux_4_uc2 	  			   <= '0';
					mux_8_uc1 	  			   <= '0';
					mux_8_uc2 	  			   <= '0';
					mux_8_uc3 	   		   <= '0';
					men_dados_uc   		   <= '0';
					read_enable_data_uc     <= '0';
					read_enable_memory_uc 	<= '0';			
					reg_uc		   		   <= '0';
			--Ciclo de espera
			when c0 =>
					mux_add_uc1 				<= '1';
					mux_add_uc2 	         <= '0';
					reg_pc_uc 		         <= '0';
					reg_d1_uc 		         <= '0';
					reg_d2_uc 		         <= '0';
					reg_d3_uc 		         <= '0';
					reg_d4_uc 		         <= '0';
					ula_uc 			         <= "00";
					concat_uc 		         <= '0';
					enable_pilha 	         <= '0';
					PUSH_barPOP 	         <= '0';
					mux_4_uc1 		         <= '0';
					mux_4_uc2 		         <= '0';
					mux_8_uc1 		         <= '0';
					mux_8_uc2 		         <= '0';
					mux_8_uc3 		         <= '0';
					men_dados_uc 	         <= '0';
					read_enable_data_uc 		<= '0';
					read_enable_memory_uc 	<= '0';
					reg_uc						<= '0';
			--Abilita registrador  uc
			when c1 =>
					mux_add_uc1 				<= '1';
					mux_add_uc2 	         <= '0';
					reg_pc_uc 		         <= '0';
					reg_d1_uc 		         <= '0';
					reg_d2_uc 		         <= '0';
					reg_d3_uc 		         <= '0';
					reg_d4_uc 		         <= '0';
					ula_uc 			         <= "00";
					concat_uc 		         <= '0';
					enable_pilha 	         <= '0';
					PUSH_barPOP 	         <= '0';
					mux_4_uc1 		         <= '0';
					mux_4_uc2 		         <= '0';
					mux_8_uc1 		         <= '0';
					mux_8_uc2 		         <= '0';
					mux_8_uc3 		         <= '0';
					men_dados_uc 	         <= '0';
					read_enable_data_uc 		<= '0';
					read_enable_memory_uc 	<= '0';
					reg_uc						<= '1';
			--ciclo de pc + 1
			when c2 =>
					mux_add_uc1 	<= '1';
					mux_add_uc2 	<= '0';
					reg_pc_uc 		<= '1';
					reg_d1_uc 		<= '0';
					reg_d2_uc 		<= '0';  
					reg_d3_uc 		<= '0';    
					reg_d4_uc 		<= '0';   
					ula_uc 			<= "00";      
					concat_uc 		<= '0';   
					enable_pilha 	<= '0';
					PUSH_barPOP 	<= '0';
					mux_4_uc1 		<= '0';
					mux_4_uc2 		<= '0';
					mux_8_uc1 		<= '0';
					mux_8_uc2 		<= '0';
					mux_8_uc3 		<= '0';
					men_dados_uc 	<= '0';
					read_enable_data_uc <='0';
					read_enable_memory_uc <='0';
					reg_uc			<= '0';
			--Adicionar na pilha valor da memoria	
			when c3 =>
					mux_add_uc1 	<= '0';
					mux_add_uc2 	<= '1';
					reg_pc_uc 		<= '0';
					reg_d1_uc 		<= '0';
					reg_d2_uc 		<= '0';  
					reg_d3_uc 		<= '0';    
					reg_d4_uc 		<= '0';   
					ula_uc 			<= "00";      
					concat_uc 		<= '0';   
					enable_pilha 	<= '1';
					PUSH_barPOP 	<= '1';
					mux_4_uc1 		<= '0';
					mux_4_uc2 		<= '1';
					mux_8_uc1 		<= '0';
					mux_8_uc2 		<= '0';
					mux_8_uc3 		<= '0';
					men_dados_uc 	<= '0';
					read_enable_data_uc <='0';
					read_enable_memory_uc <='0';
					reg_uc			<= '0';
				--registrador que recebem saida da pilha
			when c4 =>	-- REG 4
					mux_add_uc1 	<= '0';
					mux_add_uc2 	<= '1';
					reg_pc_uc 		<= '0';
					reg_d1_uc 		<= '0';
					reg_d2_uc 		<= '0';  
					reg_d3_uc 		<= '0';    
					reg_d4_uc 		<= '1';   
					ula_uc 			<= "00";      
					concat_uc 		<= '0';   
					enable_pilha 	<= '1';
					PUSH_barPOP 	<= '0';
					mux_4_uc1 		<= '0';
					mux_4_uc2 		<= '0';
					mux_8_uc1 		<= '0';
					mux_8_uc2 		<= '0';
					mux_8_uc3 		<= '0';
					men_dados_uc 	<= '0';
					read_enable_data_uc <='0';
					read_enable_memory_uc <='0';
					reg_uc			<= '0';
			when c5 =>	-- REG 3
					mux_add_uc1 	<= '0';
					mux_add_uc2 	<= '1';
					reg_pc_uc 		<= '0';
					reg_d1_uc 		<= '0';
					reg_d2_uc 		<= '0';  
					reg_d3_uc 		<= '1';    
					reg_d4_uc 		<= '0';   
					ula_uc 			<= "00";      
					concat_uc 		<= '0';   
					enable_pilha 	<= '1';
					PUSH_barPOP 	<= '0';
					mux_4_uc1 		<= '0';
					mux_4_uc2 		<= '0';
					mux_8_uc1 		<= '0';
					mux_8_uc2 		<= '0';
					mux_8_uc3 		<= '0';
					men_dados_uc 	<= '0';
					read_enable_data_uc <='0';
					read_enable_memory_uc <='0';
					reg_uc			<= '0';
			when c6 =>	-- REG 2
					mux_add_uc1 	<= '0';
					mux_add_uc2 	<= '1';
					reg_pc_uc 		<= '0';
					reg_d1_uc 		<= '0';
					reg_d2_uc 		<= '1';  
					reg_d3_uc 		<= '0';    
					reg_d4_uc 		<= '0';   
					ula_uc 			<= "00";      
					concat_uc 		<= '0';   
					enable_pilha 	<= '1';
					PUSH_barPOP 	<= '0';
					mux_4_uc1 		<= '0';
					mux_4_uc2 		<= '0';
					mux_8_uc1 		<= '0';
					mux_8_uc2 		<= '0';
					mux_8_uc3 		<= '0';
					men_dados_uc 	<= '0';
					read_enable_data_uc <='0';
					read_enable_memory_uc <='0';
					reg_uc			<= '0';
			when c7 =>	--REG 1
					mux_add_uc1 	<= '0';
					mux_add_uc2 	<= '1';
					reg_pc_uc 		<= '0';
					reg_d1_uc 		<= '1';
					reg_d2_uc 		<= '0';  
					reg_d3_uc 		<= '0';    
					reg_d4_uc 		<= '0';   
					ula_uc 			<= "00";      
					concat_uc 		<= '0';   
					enable_pilha 	<= '1';
					PUSH_barPOP 	<= '0';
					mux_4_uc1 		<= '0';
					mux_4_uc2 		<= '0';
					mux_8_uc1 		<= '0';
					mux_8_uc2 		<= '0';
					mux_8_uc3 		<= '0';
					men_dados_uc 	<= '0';
					read_enable_data_uc <='0';
					read_enable_memory_uc <='0';
					reg_uc			<= '0';
					--habilitar troca de memoria programa
			when c10 =>
					mux_add_uc1 			 <= '0';
					mux_add_uc2 			 <= '1';
					reg_pc_uc 				 <= '0';
					reg_d1_uc 				 <= '0';
					reg_d2_uc 				 <= '0';  
					reg_d3_uc 				 <= '0';    
					reg_d4_uc 				 <= '0';   
					ula_uc 					 <= "00";      
					concat_uc 				 <= '0';   
					enable_pilha 			 <= '1';
					PUSH_barPOP 			 <= '1';
					mux_4_uc1 				 <= '0';
					mux_4_uc2 				 <= '0';
					mux_8_uc1 				 <= '0';
					mux_8_uc2 			    <= '0';
					mux_8_uc3 				 <= '0';
					men_dados_uc 			 <= '0';
					read_enable_data_uc   <= '0';
					read_enable_memory_uc <= '1';
					reg_uc					 <= '0';
					--soma
			when c20 =>
					mux_add_uc1 	<= '0';
					mux_add_uc2 	<= '1';
					reg_pc_uc 		<= '0';
					reg_d1_uc 		<= '0';
					reg_d2_uc 		<= '0';  
					reg_d3_uc 		<= '0';    
					reg_d4_uc 		<= '0';   
					ula_uc 			<= "00";      
					concat_uc 		<= '0';   
					enable_pilha 	<= '1';
					PUSH_barPOP 	<= '1';
					mux_4_uc1 		<= '1';
					mux_4_uc2 		<= '0';
					mux_8_uc1 		<= '0';
					mux_8_uc2 		<= '0';
					mux_8_uc3 		<= '0';
					men_dados_uc 	<= '0';
					read_enable_data_uc <='0';
					read_enable_memory_uc <='0';
					reg_uc			<= '0';
			
			when c31 =>	-- REG 2
					mux_add_uc1 	<= '0';
					mux_add_uc2 	<= '1';
					reg_pc_uc 		<= '0';
					reg_d1_uc 		<= '0';
					reg_d2_uc 		<= '1';  
					reg_d3_uc 		<= '0';    
					reg_d4_uc 		<= '0';   
					ula_uc 			<= "00";      
					concat_uc 		<= '0';   
					enable_pilha 	<= '0';
					PUSH_barPOP 	<= '0';
					mux_4_uc1 		<= '0';
					mux_4_uc2 		<= '0';
					mux_8_uc1 		<= '0';
					mux_8_uc2 		<= '0';
					mux_8_uc3 		<= '0';
					men_dados_uc 	<= '0';
					read_enable_data_uc <='0';
					read_enable_memory_uc <='0';
					reg_uc			<= '0';
			when c30 =>	--REG 1
					mux_add_uc1 	<= '0';
					mux_add_uc2 	<= '1';
					reg_pc_uc 		<= '0';
					reg_d1_uc 		<= '1';
					reg_d2_uc 		<= '0';  
					reg_d3_uc 		<= '0';    
					reg_d4_uc 		<= '0';   
					ula_uc 			<= "00";      
					concat_uc 		<= '0';   
					enable_pilha 	<= '0';
					PUSH_barPOP 	<= '0';
					mux_4_uc1 		<= '0';
					mux_4_uc2 		<= '0';
					mux_8_uc1 		<= '0';
					mux_8_uc2 		<= '0';
					mux_8_uc3 		<= '0';
					men_dados_uc 	<= '0';
					read_enable_data_uc <='0';
					read_enable_memory_uc <='0';
					reg_uc			<= '0';
				when c32 =>	-- REG 2
					mux_add_uc1 	<= '0';
					mux_add_uc2 	<= '1';
					reg_pc_uc 		<= '0';
					reg_d1_uc 		<= '0';
					reg_d2_uc 		<= '1';  
					reg_d3_uc 		<= '0';    
					reg_d4_uc 		<= '0';   
					ula_uc 			<= "00";      
					concat_uc 		<= '0';   
					enable_pilha 	<= '1';
					PUSH_barPOP 	<= '0';
					mux_4_uc1 		<= '0';
					mux_4_uc2 		<= '0';
					mux_8_uc1 		<= '0';
					mux_8_uc2 		<= '0';
					mux_8_uc3 		<= '0';
					men_dados_uc 	<= '0';
					read_enable_data_uc <='0';
					read_enable_memory_uc <='0';
					reg_uc			<= '0';
			when c33 =>	--REG 1
					mux_add_uc1 	<= '0';
					mux_add_uc2 	<= '1';
					reg_pc_uc 		<= '0';
					reg_d1_uc 		<= '1';
					reg_d2_uc 		<= '0';  
					reg_d3_uc 		<= '0';    
					reg_d4_uc 		<= '0';   
					ula_uc 			<= "00";      
					concat_uc 		<= '0';   
					enable_pilha 	<= '1';
					PUSH_barPOP 	<= '0';
					mux_4_uc1 		<= '0';
					mux_4_uc2 		<= '0';
					mux_8_uc1 		<= '0';
					mux_8_uc2 		<= '0';
					mux_8_uc3 		<= '0';
					men_dados_uc 	<= '0';
					read_enable_data_uc <='0';
					read_enable_memory_uc <='0';
					reg_uc			<= '0';
			end case;
	end process;
end rtl;
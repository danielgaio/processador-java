-- Este é um mux que fica na saida da pilha

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity mux_8to1 is
 port(
	-- entradas
	A, B, C, D, E, F, G, H 	: in std_logic_vector(7 downto 0);
	-- seleção
   S0, S1, S2					: in STD_LOGIC;
	-- saida
   saida							: out std_logic_vector(7 downto 0)
  );
end mux_8to1;

architecture rtl of mux_8to1 is
begin
	process (A, B, C, D, E, F, G, H, S0, S1, S2) is
	begin
		if (S2 = '0' and S1 = '0' and S0 ='0') then
			saida <= A;
		elsif (S2 = '0' and S1 = '0' and S0 ='1') then
			saida <= B;
		elsif (S2 = '0' and S1 = '1' and S0 ='0') then
			saida <= C;
		elsif (S2 = '0' and S1 = '1' and S0 ='1') then
			saida <= D;
		elsif (S2 = '1' and S1 = '0' and S0 ='0') then
			saida <= E;
		elsif (S2 = '1' and S1 = '0' and S0 ='1') then
			saida <= F;
		elsif (S2 = '1' and S1 = '1' and S0 ='0') then
			saida <= G;
		else
			saida <= H;
		end if;

	end process;
end rtl;
-- Quartus II VHDL Template
-- Single port RAM with single read/write address

library ieee;
use ieee.std_logic_1164.all;

entity single_port_ram is

	generic
	(
		DATA_WIDTH : natural 	:= 8; 	-- Tamanho dos dados
		ADDRESS_WIDTH : natural := 6 	-- Tamanho do endereço (define o tamanho da memória)
	);

	port 
	(
		clk				: in std_logic;
												 -- ^
		address			: in natural range 0 to 2**ADDRESS_WIDTH - 1; 	-- 2 elevado ao tamanho de endereço - 1, address é o indexador da memória
																							-- a cada borda será inviado para a saída o conteúdo da posição address
		data				: in std_logic_vector((DATA_WIDTH-1) downto 0);
		write_enable	: in std_logic := '1';
		ram_output		: out std_logic_vector((DATA_WIDTH -1) downto 0)
	);

end entity;

architecture rtl of single_port_ram is

	-- Build a 2-D array type for the RAM
	subtype word_t is std_logic_vector((DATA_WIDTH-1) downto 0); -- word_t é um novo tipo, que é de tamanho data_with, esse é o tipo para as 
																					 -- palavras de memória
	type memory_t is array(2**ADDRESS_WIDTH-1 downto 0) of word_t; -- memory_t é um novo tipo de variavel, para a memória toda, de forma global.

	-- Declare the RAM signal.	
	signal ram : memory_t;

	-- Registrador para guardar o endereço 
	signal address_register : natural range 0 to 2**ADDRESS_WIDTH-1;

begin

	process(clk)
	begin
	if(rising_edge(clk)) then
		-- se o enable de escrita em memória esta ativo, grava na ram o conteudo no endereço especificado
		if(write_enable = '1') then
			ram(address) <= data;
		end if;

		-- Register the address for reading
		address_register <= address;
	end if;
	end process;

	ram_output <= ram(address_register);

end rtl;

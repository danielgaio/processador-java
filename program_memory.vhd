-- Quartus II VHDL Template
-- Single port RAM with single read/write address

library ieee;
use ieee.std_logic_1164.all;

entity program_memory is

	generic
	(
		DATA_WIDTH 		: natural := 8; 		-- Tamanho dos dados
		ADDRESS_WIDTH 	: natural := 6 		-- Tamanho do endereÃ§o (define o tamanho da memÃ³ria)
	);

	port 
	(
		clk						: in std_logic;
		address					: in natural range 0 to 2**ADDRESS_WIDTH - 1; 	-- 2 elevado ao tamanho de endereÃ§o - 1, address Ã© o indexador da memÃ³ria
																							-- a cada borda serÃ¡ inviado para a saÃ­da o conteÃºdo da posiÃ§Ã£o address
		data						: in std_logic_vector((DATA_WIDTH-1) downto 0);
		write_enable			: in std_logic := '1';
		read_enable_program	: in std_logic;
		program_output			: out std_logic_vector((DATA_WIDTH -1) downto 0)
	);

end entity;

architecture rtl of program_memory is

	-- Build a 2-D array type for the RAM
	subtype word_t is std_logic_vector((DATA_WIDTH-1) downto 0); -- word_t Ã© um novo tipo, que Ã© de tamanho data_with, esse Ã© o tipo para as palavras de memoria
	type memory_t is array(2**ADDRESS_WIDTH-1 downto 0) of word_t; -- memory_t Ã© um novo tipo de variavel, para a memÃ³ria toda, de forma global.


	-- Declare the RAM signal.	
	signal ram : memory_t := (
		0 => "00010000",	-- bipush
		1 => "00000111",	-- numero to add
		2 => "00010000",	-- bipush
		3 => "00000010",	-- numero to add
		4 => "01100000",	-- iadd
		5 => "00010000",	-- bipush
		6 => "00000111",	-- numero to add
		7 => "01100000",
		OTHERS => "00000000"
	);

	-- Registrador para guardar o endereÃ§o 
	signal address_register : natural range 0 to 2**ADDRESS_WIDTH-1;

begin

	process(write_enable, read_enable_program)
	begin
	if(rising_edge(clk)) then
		-- se o enable de escrita em memÃ³ria esta ativo, grava na ram o conteudo no endereÃ§o especificado
		if(write_enable = '1') then
			ram(address) <= data;
		end if;

		-- Register the address for reading
		if(read_enable_program = '1') then
			address_register <= address;
		end if;
	end if;
	end process;

	program_output <= ram(address_register);

end rtl;

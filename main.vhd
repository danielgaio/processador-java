-- arquivo principal do projeto, que vai agregar todos os componentes

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity main is
	generic
	(
		DATA_WIDTH 		: natural := 8;
		ADDRESS_WIDTH 	: natural := 6
	);
	port(
		-- portas gerais, a maioria para visualização e debug
		clock_ext					: in std_logic;													-- clock global
		reset_s						: in std_logic;
		saida_memoria_programa  : out std_logic_vector((DATA_WIDTH-1) downto 0);
		saida_pilha        		: out std_logic_vector((DATA_WIDTH-1) downto 0);
		entrada_pilha        	: out std_logic_vector((DATA_WIDTH-1) downto 0);
		pc		         			: out std_logic_vector(31 downto 0);
		-- ++++++++++++++++++++++++++++
		out_mux		         	: out std_logic_vector(31 downto 0);
		pilha_empty					: out std_logic;
		-- ++++++++++++++++++++++++++++
		controleInsrucao			: out std_logic_vector((DATA_WIDTH-1) downto 0);
		controleStatus				: out std_logic_vector((DATA_WIDTH-1) downto 0);
		sr1							: out std_logic_vector((DATA_WIDTH-1) downto 0);
		sr2							: out std_logic_vector((DATA_WIDTH-1) downto 0);
		sr3							: out std_logic_vector((DATA_WIDTH-1) downto 0);
		sr4							: out std_logic_vector((DATA_WIDTH-1) downto 0)
	);
end entity;

architecture hardware of main is

-------------------- DECLARAÇÃO DE SINAIS ----------------

-- sinais da memória de programa
signal memory_data		: std_logic_vector((DATA_WIDTH-1) downto 0);
signal we_ram				: std_logic;
signal out_ram				: std_logic_vector((DATA_WIDTH -1) downto 0);
signal program_output	: std_logic_vector((DATA_WIDTH -1) downto 0);

-- sinais da pilha  								
signal reset_pilha	: std_logic; 																  	
signal reg_out_pilha	: std_logic_vector((DATA_WIDTH -1) downto 0);  	
signal selecao_pilha	: std_logic;  								
signal flag_full		: std_logic;  							
signal flag_empty		: std_logic;

-- sinais do mux 4 x 1 - 8 bits
signal A4, B4, C4, D4	: std_logic_vector (7 downto 0);
signal S0_4, S1_4			: std_logic;
signal saida4				: std_logic_vector(7 downto 0);

-- sinais do mux 4 x 1 - 32 bits
signal A4_32, B4_32, C4_32, D4_32	: std_logic_vector (((DATA_WIDTH*4)-1) downto 0);
signal S0_4_32, S1_4_32					: std_logic;
signal saida4_32							: std_logic_vector(((DATA_WIDTH*4)-1) downto 0);

-- sinais do mux 8 x 1
signal A8, B8, C8, D8, E8, F8, G8, H8	: std_logic_vector (7 downto 0);
signal S0_8, S1_8, S2_8	  					: std_logic;
signal saida8				  					: std_logic_vector(7 downto 0);

-- sinais do demux de saida da pilha
signal D_entrada				: STD_LOGIC;
signal D_S0, D_S1				: STD_LOGIC;
signal D_A, D_B, D_C, D_D	: STD_LOGIC;

-- sinais da ula
signal entrada_a		:  std_logic_vector ((DATA_WIDTH-1) downto 0);		
signal entrada_b     :  std_logic_vector ((DATA_WIDTH-1) downto 0);
signal ula_seletor   :  std_logic_vector (1 downto 0);
signal ula_result    :  std_logic_vector (((DATA_WIDTH)-1) downto 0);
signal flag				:  std_logic_vector (1 downto 0);

-- sinais para registradores
signal register_1, register_2, register_3, register_4 : std_logic_vector(7 downto 0);
signal register_pc, pc_out_mux		: std_logic_vector(31 downto 0);

-- sinais para a memória de dados
signal address					: natural range 0 to 2**ADDRESS_WIDTH - 1;
signal data_input          : std_logic_vector((DATA_WIDTH-1) downto 0);
signal write_enable        : std_logic := '1';
signal data_memory_output  : std_logic_vector((DATA_WIDTH -1) downto 0);

-- sinais do controle
signal input_s					: std_logic_vector(7 downto 0) ;
signal reg_pc_uc,
		 reg_1_uc,
		 reg_2_uc,
		 reg_3_uc,
		 reg_4_uc,
		 read_e_p_uc,
		 read_e_d_uc,
		 enable_pilha,
		 flag_mudanca_saida_pilha
: std_logic;

-- sinais do concatenador
signal concat_uc		: std_logic;
signal concat_output : std_logic_vector(((DATA_WIDTH*4)-1) downto 0);

------------- DECLARAÇÃO DOS COPONENTES ----------------

-- MEMÓRIA DE PROGRAMA
component program_memory
	port(
		clk						: in std_logic;
		address					: in natural range 0 to 2**ADDRESS_WIDTH - 1;
		data						: in std_logic_vector((DATA_WIDTH-1) downto 0);
		write_enable			: in std_logic := '1';
		read_enable_program	: in std_logic;
		program_output			: out std_logic_vector((DATA_WIDTH -1) downto 0)
	);
end component;

-- PILHA
component stack
	port(
		Clk 			: in std_logic;  								--Clock for the stack.
      Reset 		: in std_logic; 								--active high reset.    
      Enable 		: in std_logic;  								--Enable the stack. Otherwise neither push nor pop will happen.
      Data_In 		: in std_logic_vector(7 downto 0);  	--Data to be pushed to stack
      Data_Out 	: out std_logic_vector(7 downto 0);  	--Data popped from the stack.
      PUSH_POP_SEL: in std_logic;  								--active low for POP and active high for PUSH.
      Stack_Full 	: out std_logic;  							--Goes high when the stack is full.
      Stack_Empty : out std_logic  								--Goes high when the stack is empty.
   );
end component;

-- MUX 4 x 1
component mux_4to1
	port (
		A, B, C, D 	: in std_logic_vector (7 downto 0);
		S0, S1		: in std_logic;
		saida			: out std_logic_vector(7 downto 0)
	);
end component;

-- MUX 4 x 1 - 32 BITS
component mux_4to1_32
	port (
		A, B, C, D 	: in std_logic_vector (((DATA_WIDTH*4)-1) downto 0);
		S0, S1		: in std_logic;
		saida			: out std_logic_vector(((DATA_WIDTH*4)-1) downto 0)
	);
end component;

-- MUX 8 x 1
component mux_8to1
	port (
		A, B, C, D, E, F, G, H 	: in std_logic_vector (7 downto 0);
		S0, S1, S2					: in std_logic;
		saida							: out std_logic_vector(7 downto 0)
	);
end component;

-- DEMUX
component demux_1to4
	port(
		entrada 		: in std_logic;
		S0, S1		: in std_logic;
		A, B, C, D	: out std_logic
	);
end component;

-- ULA
component ula
	port(
		input_a	: in std_logic_vector ((DATA_WIDTH-1) downto 0);		
		input_b	: in std_logic_vector ((DATA_WIDTH-1) downto 0);
		addSubMul: in std_logic_vector (1 downto 0);
		result 	: out std_logic_vector (((DATA_WIDTH)-1) downto 0);
		flag 		: out std_logic_vector (1 downto 0)
	);
end component;

-- DATA_MEMORY
component data_memory
	port(
		clk						: in std_logic;
		address_data			: in natural range 0 to 2**ADDRESS_WIDTH - 1;
		data_input				: in std_logic_vector((DATA_WIDTH-1) downto 0);
		write_enable			: in std_logic := '1';
		read_enable_data		: in std_logic;
		data_memory_output	: out std_logic_vector((DATA_WIDTH -1) downto 0)
	);
end component;

-- CONTROLE
component controle
	port(											
		clk										: in	std_logic;
		input	 							      : in	std_logic_vector(7 downto 0);
		reset	 							      : in	std_logic;
		ula_input 						      : in	std_logic_vector(1 downto 0);
		mux_add_uc1, mux_add_uc2 	      : out	std_logic;
		reg_pc_uc						      : out	std_logic;
		reg_d1_uc						      : out std_logic;
		reg_d2_uc						      : out	std_logic;
		reg_d3_uc						      : out	std_logic;
		reg_d4_uc						      : out	std_logic;
		ula_uc							      : out	std_logic_vector(1 downto 0);
		concat_uc								: out	std_logic;
		enable_pilha					      : out	std_logic;
		PUSH_barPOP						      : out	std_logic;
		mux_4_uc1, mux_4_uc2			      : out	std_logic;
		mux_8_uc1, mux_8_uc2, mux_8_uc3	: out	std_logic;
		men_dados_uc							: out	std_logic;
		read_enable_data_uc					: out	std_logic;
		read_enable_memory_uc				: out	std_logic;
		r2							 				: out	std_logic_vector(7 downto 0);
		rstatus									: out	std_logic_vector(7 downto 0)
	);
end component;

-- CONCATENADOR
component concat32
	port(
		concat_uc		: in std_logic;
		reg1				: in std_logic_vector((DATA_WIDTH-1) downto 0);
		reg2				: in std_logic_vector((DATA_WIDTH-1) downto 0);
		reg3				: in std_logic_vector((DATA_WIDTH-1) downto 0);
		reg4				: in std_logic_vector((DATA_WIDTH-1) downto 0);
		concat_output	: out std_logic_vector(((DATA_WIDTH*4)-1) downto 0)
	);
end component;

--============= CORPO ARCHITECTURE ==============
begin
		-- sinais externos para debug e vidualização
		saida_memoria_programa	<= program_output;
		entrada_pilha				<= saida8;
		saida_pilha 				<= reg_out_pilha;
		pc								<= register_pc;
		sr1							<=	register_1;
		sr2							<=	register_2;
		sr3							<=	register_3;
		sr4							<=	register_4;
		out_mux						<= pc_out_mux;
		pilha_empty					<= flag_empty;

	-- declaração de um componente para a memória de programa
	my_program_memory: program_memory port map(
		clk 						=> clock_ext,
		address 					=> to_integer(unsigned(register_pc)),
		data 						=> memory_data,
		write_enable 			=> we_ram,
		read_enable_program	=> read_e_p_uc,
		program_output 		=> program_output
	);
	-- declaração de um componente pilha
	my_pilha: stack port map(
		Clk 				=>	clock_ext,
      Reset 		   =>	reset_s,
      Enable 		   =>	enable_pilha,
      Data_In 		   =>	saida8,
      Data_Out 	   =>	reg_out_pilha, 							--saida_pilha
      PUSH_POP_SEL	=> selecao_pilha,
      Stack_Full 		=> flag_full,
      Stack_Empty  	=> flag_empty		
	);
	
	-- registrador R1
	process(clock_ext)
	begin
		if clock_ext'event and clock_ext='1' then
			if (reg_1_uc = '1') then									-- sinal de controle
				register_1 <= reg_out_pilha;
			end if;
		end if;
	end process;
	
	-- registrador R2
	process(clock_ext)
	begin
		if clock_ext'event and clock_ext='1' then
			if (reg_2_uc = '1') then									-- sinal de controle
				register_2 <= reg_out_pilha;
			end if;
		end if;
	end process;
	
	-- registrador R3
	process(clock_ext)
	begin
		if clock_ext'event and clock_ext='1' then
			if (reg_3_uc = '1') then									-- sinal de controle
				register_3 <= reg_out_pilha;
			end if;
		end if;
	end process;
	
	-- registrador R4
	process(clock_ext)
	begin
		if clock_ext'event and clock_ext='1' then
			if (reg_4_uc = '1') then									-- sinal de controle
				register_4 <= reg_out_pilha;
			end if;
		end if;
	end process;
	
	-- mux_4_uc que fica na entrada do mux 8 da pilha
	mux_4: mux_4to1 port map(
		A 	 	=> data_memory_output,										-- saida da memória de dados
		B 	 	=> ula_result, 												-- saida da ula
		C 	 	=> program_output,				
		D 	 	=> D4,
		S0 	=> S0_4,
		S1 	=> S1_4,
		saida => saida4
	);
	
	-- mux_4_uc que fica antes do registrador pc
	mux_4_pc: mux_4to1_32 port map(
		A 	 	=> concat_output,											-- saida do concat
		B 	 	=> std_logic_vector(unsigned(register_pc) + 1),
		C 	 	=> "00000000000000000000000000000000",				-- para o reset da arquitetura	
		D 	 	=> D4_32,
		S0 	=> S0_4_32,
		S1 	=> S1_4_32,
		saida => pc_out_mux
	);
	
	-- registrador PC & PC + 1
	process(clock_ext)
	begin	
		if clock_ext'event and clock_ext = '1' then
			if (reg_pc_uc = '1') then									-- sinal de controle
				register_pc <= pc_out_mux;
			end if;
		end if;
	end process;
	
	-- mux_8_uc
	mux_8: mux_8to1 port map(
		A 			=> saida4,										-- saida do mux 4x1
		B 			=> "10000001",                         -- aqui é -1
		C 			=> "00000000",	                        -- constantes para algumas instruções
		D 			=> "00000001",
		E 			=> "00000010",
		F 			=> "00000011",
		G 			=> "00000100",
		H 			=> "00000101",
		S0 		=> S0_8,
		S1 		=> S1_8,
		S2 		=> S2_8,
		saida 	=> saida8
	);
	
	-- componente ula
	ula_component: ula port map(
		input_a	 	=> register_1,
		input_b	 	=> register_2,
		addSubMul 	=> ula_seletor,
		result 	 	=> ula_result,
		flag 			=> flag
	);
	
	-- componente da memória de dados
	my_data_memory: data_memory port map(
		clk						=> clock_ext,
		address_data			=> to_integer(unsigned(register_2)),	--converção do endereço (std_logic_vector) que entra no componente de memória, para (integer=natural), formato interno da memória
		data_input				=> register_1,          
		write_enable		   => write_enable,
		read_enable_data		=> read_e_d_uc,		   
		data_memory_output   => data_memory_output
	);
	
--	flag_mudanca_saida_pilha <= '1' when (register_1 = reg_out_pilha) else '0';

	-- componente de controle1
	my_control: controle port map(
		clk	 						=> clock_ext,
		input	 					   => program_output,
		reset	 					   => reset_s,
		ula_input 				   => flag, 
		mux_add_uc1 			   => S0_4_32,
		mux_add_uc2				   => S1_4_32,
		reg_pc_uc 				   => reg_pc_uc,
		reg_d1_uc 				   => reg_1_uc,    
		reg_d2_uc 				   => reg_2_uc,
		reg_d3_uc 				   => reg_3_uc,
		reg_d4_uc 				   => reg_4_uc,
		ula_uc 					   => ula_seletor,  
		concat_uc   			   => concat_uc,
		enable_pilha			   =>	enable_pilha,
		PUSH_barPOP 			   =>	selecao_pilha,
		mux_4_uc1				   =>	S0_4,
		mux_4_uc2				   =>	S1_4, 
		mux_8_uc1				   =>	S0_8,
		mux_8_uc2				   =>	S1_8,
		mux_8_uc3				   =>	S2_8,
		men_dados_uc			   => write_enable,
		read_enable_data_uc     => read_e_d_uc,
		read_enable_memory_uc   => read_e_p_uc,
		r2								=>	controleInsrucao, 				
		rstatus					   => controleStatus

	);
	
	-- componente concatenador
	my_concat: concat32 port map(
		concat_uc		=> concat_uc,
		reg1	   		=> register_1, 
		reg2	   		=> register_2, 
		reg3	   		=> register_3, 
		reg4	   		=> register_4,
		concat_output	=> concat_output
	);
end hardware;
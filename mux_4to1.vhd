-- Este é um mux que fica na entrada da pilha


-- tem um mux 8 x 1 e outro 4 x 1, então esse arquivo vai ser o 4 x 1
-- e tem que criar outro mux pra ser o 8 x 1

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity mux_4to1 is
 port(
	-- entradas
	A, B, C, D 	: in std_logic_vector(7 downto 0);
	-- seleção
   S0, S1		: in STD_LOGIC;
	-- saida
   saida			: out std_logic_vector(7 downto 0)
  );
end mux_4to1;

architecture rtl of mux_4to1 is
begin
	process (A, B, C, D, S0, S1) is
	begin
		if (S0 ='0' and S1 = '0') then
			saida <= A;
		elsif (S0 ='1' and S1 = '0') then
			saida <= B;
		elsif (S0 ='0' and S1 = '1') then
			saida <= C;
		else
			saida <= D;
		end if;

	end process;
end rtl;
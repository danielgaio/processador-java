library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity demux_1to4 is
	port(
		entrada 		: in STD_LOGIC;
		S0, S1		: in STD_LOGIC;
		A, B, C, D	: out STD_LOGIC
	);
end demux_1to4;

architecture bhv of demux_1to4 is
begin
	process (entrada, S0, S1) is
	begin
		if (S0 = '0' and S1 = '0') then
			A <= entrada;
		elsif (S0 ='1' and S1 = '0') then
			B <= entrada;
		elsif (S0 ='0' and S1 = '1') then
			C <= entrada;
		else
			D <= entrada;
		end if;

	end process;
end bhv;
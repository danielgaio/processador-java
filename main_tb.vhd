library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity main_tb is

end entity;

architecture testbench of main_tb is

-- COMPONENTE
component main is
		port(
			clock_ext: in std_logic
		);
	end component;
	
-- SINAIS
signal clock_ext : std_logic := '0';

-- inicio do corpo da architecture
begin
	-- instanciado um componente do main
	my_main_top : main port map(
		clock_ext => clock_ext
	);
	-- gerando o clock
	clock_ext <= not clock_ext after 10 ns;
end testbench;